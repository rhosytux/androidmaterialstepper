package com.elangtech.androidstepperui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by ElangTECH on 11/28/18.
 */
public class StepFragmentAdapter extends AbstractFragmentStepAdapter {

    public StepFragmentAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {

        switch (position) {
            case 0:
                StepOneFragment stepOneFragment = new StepOneFragment();
                Bundle b = new Bundle();
                b.putInt("STEP_ONE", position);
                stepOneFragment.setArguments(b);
                return stepOneFragment;
            case 1:
                StepTwoFragment stepTwoFragment = new StepTwoFragment();
                Bundle b2 = new Bundle();
                b2.putInt("STEP_TWO", position);
                stepTwoFragment.setArguments(b2);
                return stepTwoFragment;
            case 2:
                StepThreeFragment stepThreeFragment = new StepThreeFragment();
                Bundle b3 = new Bundle();
                b3.putInt("STEP_THREE", position);
                stepThreeFragment.setArguments(b3);
                return stepThreeFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {
        switch (position){
            case 0:
                return new StepViewModel.Builder(context).setTitle("1").create();
            case 1:
                return new StepViewModel.Builder(context).setTitle("2").create();
            case 2:
                return new StepViewModel.Builder(context).setTitle("3").create();
        }
        return super.getViewModel(position);
    }
}
